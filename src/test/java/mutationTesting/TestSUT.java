package mutationTesting;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

class TestSUT {


	@Test
	void testAjoutDe5éléments() throws TableauPleinException {
		SUT sut=new SUT(5);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		sut.ajout(4);
		sut.ajout(5);
		int[] tab=sut.values();
		int[] expected = {1,2,3,4,5};
		assertArrayEquals(expected,tab);
		assertEquals(5,sut.values().length);
	}

	@Test 
	void testExceptionQuandAjoutDansTableauPlein() throws TableauPleinException {
		SUT sut=new SUT(3);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		assertThrows(TableauPleinException.class, ()->{sut.ajout(4);});
	}
	
	@Test
	void testMinTableauRempliTrié() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(4);
		sut.ajout(1);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertTrue(ArrayUtils.contains(sut.values(), 1));
		assertEquals(3,sut.values().length);
	}
	
	@Test 
	void testretourneEtSupprimePremiereOccurenceMin() throws TableauPleinException, TableauVideException
	{
		SUT sut=new SUT(2);
		sut.ajout(1);
		sut.ajout(2);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		assertEquals(1, sut.values().length);
		
	}
	
	@Test void testretourneEtSupprimePremiereOccurenceMinNewValeur() throws TableauPleinException, TableauVideException
	{
		SUT sut=new SUT(4);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(1);
		sut.ajout(3);
		

		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertTrue(ArrayUtils.contains(sut.values(), 1));
		assertEquals(3, sut.values().length);

		assertEquals(sut.values()[0],2);
		assertEquals(sut.values()[1],1);
		assertEquals(sut.values()[2],3);
		
	}

	@Test
	void testMinTableauRempliNonTrié() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(3);
		sut.ajout(5);
		sut.ajout(1);
		sut.ajout(2);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		assertEquals(2,sut.values().length);
	}
	
	@Test
	void testMinTableauVide() {
		SUT sut=new SUT(3);
		assertThrows(TableauVideException.class, ()->{sut.retourneEtSupprimePremiereOccurenceMin();});
	}
	
	
}
